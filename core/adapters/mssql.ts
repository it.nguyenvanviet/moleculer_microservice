export const settingMSSQL: any = {
	type: "mssql",
	host: process.env.SQL_URL,
	username: process.env.SQL_USER,
	password: process.env.SQL_PASS,
	database: process.env.SQL_DB,
	extra: {
		trustServerCertificate: true
	},
    schema : "dbo",
    synchronize: false,
	migrationsRun: false
};
