import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";
@Entity()
export class Project {
	@PrimaryGeneratedColumn({ type: "int" })
	ID: number;

	@Column("nvarchar", { length: 450 })
	Name: string; 
    @Column({ type: "int" })
	Status: number;
    
    @Column("varchar", { length: 450 })
	CreateBy: string;

    @Column({ type: "datetime" })
	CreateDate: Date;

    @Column("varchar", { length: 450 })
	ModifyBy: string;

    @Column( { type: "datetime" })
	ModifyDate: Date;
 
}
