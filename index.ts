/* eslint-disable @typescript-eslint/no-base-to-string */
/* eslint-disable @typescript-eslint/no-floating-promises */
/* eslint-disable node/no-path-concat */
/* eslint-disable import/first */
/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable import/order */
const argv = require('yargs').argv
require('dotenv').config({ path: argv.env })
import OS from 'os'
import 'reflect-metadata'
import { ServiceBroker } from 'moleculer'
import fs from 'fs'
import path from 'path'

process.env.UV_THREADPOOL_SIZE = (OS.cpus().length - 1).toString()
const loadMoleculerConfig = import(process.env.MOLERCULER_CONFIG as string)

runServer()

function runServer (): void {
  loadMoleculerConfig.then((brokerConfig) => {
    const broker = new ServiceBroker(brokerConfig)
    broker.logger.info('Server is starting')
    fs.readdir(
      __dirname + path.sep + (process.env.SERVICES as string).replace(/\//, path.sep),
      function (err, files) {
        if (err != null) {
          // eslint-disable-next-line @typescript-eslint/restrict-plus-operands
          return console.log('Unable to scan directory: ' + err)
        }
        files.forEach(function (file) {
          broker.loadServices(
            __dirname +
            path.sep +
            (process.env.SERVICES as string).replace(/\//, path.sep) +
            path.sep +
            file +
            path.sep,
            '*.service.*'
          )
        })
      }
    )
    broker.start().then(() => {
      broker.logger.info('Server is started')
      broker.repl()
    })
  })
}
