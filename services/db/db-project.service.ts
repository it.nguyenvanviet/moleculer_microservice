"use strict";
import { TypeOrmDbAdapter } from "moleculer-db-adapter-typeorm";
import * as storeService from "moleculer-db";
import { Service, ServiceBroker, Context } from "moleculer";
import { settingAdapter } from "../../core/adapter";
import { Project } from "../../entity/Project";

export default class ProjectService extends Service {
	public constructor(public broker: ServiceBroker) {
		super(broker);
		this.parseServiceSchema({
			name: "project",
			adapter: new TypeOrmDbAdapter({
				...settingAdapter,
			}),
			mixins: [storeService],
			model: Project,
			settings: {
				idField: "id",
			},
		});
	}
}
