"use strict";
import { TypeOrmDbAdapter } from "moleculer-db-adapter-typeorm";
import * as storeService from "moleculer-db";
import { Service, ServiceBroker, Context } from "moleculer";
import { settingAdapter } from "../../core/adapter";
import { Team } from "../../entity/Team";

export default class TeamService extends Service {
	public constructor(public broker: ServiceBroker) {
		super(broker);
		this.parseServiceSchema({
			name: "team",
			adapter: new TypeOrmDbAdapter({
				...settingAdapter,
			}),
			mixins: [storeService],
			model: Team,
			settings: {
				idField: "ID",
			},
		});
	}
}
